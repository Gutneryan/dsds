﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestNorthWind.Core;
using TestNorthWind.Core.BussinessLayerInterfaces;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.BLL
{
    public class CategoryBL : ICategoryBL
    {
        private readonly IRepositoryManager _repositoryManager;

        public CategoryBL(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }
        public async Task AddCategory(Category categories)
        {
            _repositoryManager.Categories.Add(categories);
            await _repositoryManager.CompleteAsync();
        }

        public async Task DeleteCategory(long categoryId)
        {
            var category = await _repositoryManager.Categories.GetSingle(p => p.CategoryId == categoryId);
            if (category == null)
            {
                throw new Exception("Not Found");
            }
            _repositoryManager.Products.DeleteWhere(p => p.CategoryId == categoryId);
            _repositoryManager.Categories.Delete(category);
            await _repositoryManager.CompleteAsync();
        }

        public async Task EditCategory(long Id, Category category)
        {
            var existingCategory = await _repositoryManager.Categories.GetSingle(x => x.CategoryId == Id);
            if (existingCategory == null)
            {
                throw new Exception("Not Found");
            }
            if (string.IsNullOrEmpty(category.CategoryName) && string.IsNullOrEmpty(category.Description))
            {
                throw new Exception("All fields are empty");
            }
            else
            {

                if (!string.IsNullOrEmpty(category.CategoryName))
                    existingCategory.CategoryName = category.CategoryName;
                if (!string.IsNullOrEmpty(category.Description))
                    existingCategory.Description = category.Description;
                    existingCategory.Picture = category.Picture;
            }
            _repositoryManager.Categories.Update(existingCategory);
            await _repositoryManager.CompleteAsync();
        }

        public async Task<IEnumerable<Category>> GetAll()
        {
            return await _repositoryManager.Categories.GetAllAsync();
        }

        public async Task<Category> GetCategory(long categoryId)
        {
            var category = await _repositoryManager.Categories.GetSingle(x => x.CategoryId == categoryId);
            if (category == null)
            {
                throw new Exception("Not Found");
            }
            return category;
        }
    }
}
