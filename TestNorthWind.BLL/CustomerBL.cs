﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestNorthWind.Core;
using TestNorthWind.Core.BussinessLayerInterfaces;
using TestNorthWind.Core.Entities;
using TestNorthWind.ViewModels;

namespace TestNorthWind.BLL
{
    public class CustomerBL : ICustomerBL
    {
        private readonly IRepositoryManager _repositoriManager;
        public CustomerBL(IRepositoryManager repositoriManager)
        {
            _repositoriManager = repositoriManager;
        }

      
        public async Task AddCustomer(Customer customer)
        {
            _repositoriManager.Customers.Add(customer);
            await _repositoriManager.CompleteAsync();
        }

        public async Task AddCustomerDemographic(CustomerDemographic customer)
        {
            _repositoriManager.CustomerDemographics.Add(customer);
            await _repositoriManager.CompleteAsync();
        }

        public async Task DeleteCustomer(long customerId)
        {
            var customer = await _repositoriManager.Customers.GetSingle(p => p.CustomerId == customerId);
            if (customer==null)
            {
                throw new Exception("Not Found");
            }
            _repositoriManager.Orders.DeleteWhere(p => p.CustomerId == customerId);
            _repositoriManager.CustomerCustomerDemo.DeleteWhere(p => p.CustomerId == customerId);
            _repositoriManager.Customers.Delete(customer);
            await _repositoriManager.CompleteAsync();
        }

        public async Task EditCustomer(long Id,Customer Customer)
        {
            var customer = await _repositoriManager.Customers.GetSingle(p => p.CustomerId == Id);
            if (customer == null)
            {
                throw new Exception("Not Found");
            }
            customer.CompanyName = Customer.CompanyName;
            _repositoriManager.Customers.Update(customer);
            await _repositoriManager.CompleteAsync();

        }

        public async Task<IEnumerable<Customer>> GetAll()
        {
            return await _repositoriManager.Customers.GetAllAsync();
        }

        public async Task<Customer> GetCustomer(long customerId)
        {
            var customer = await _repositoriManager.Customers.GetSingle(p => p.CustomerId == customerId);
            if (customer == null)
            {
                throw new Exception("Not found");
            }
            return customer;
        }
    }
}
