﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.DAL.EntityConfigurations;

namespace TestNorthWind.DAL
{
    public class TNWContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<CustomerCustomerDemo> CustomerCustomerDemo { get; set; }
        public DbSet<CustomerDemographic> CustomerDemographics { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeTerritory> EmployeesTerritories { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Region> Region { get; set; }
        public DbSet<Shipper> Shippers { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Territory> Territories { get; set; }
        public TNWContext(DbContextOptions<TNWContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new CategoriesConfig());
            builder.ApplyConfiguration(new CustomerCustomerDemoConfig());
            builder.ApplyConfiguration(new CustomerDemographicsConfig());
            builder.ApplyConfiguration(new CustomersConfig());
            builder.ApplyConfiguration(new EmployeesConfig()); 
            builder.ApplyConfiguration(new EmployeesTerritoriesConfig());
            builder.ApplyConfiguration(new OrderDetailsConfig());
            builder.ApplyConfiguration(new OrdersConfig()); 
            builder.ApplyConfiguration(new ProductsConfig());
            builder.ApplyConfiguration(new RegionConfig()); 
            builder.ApplyConfiguration(new ShippersConfig()); 
            builder.ApplyConfiguration(new TerritoriesConfig());
            builder.ApplyConfiguration(new SuppliersConfig());
        }
    }
}
