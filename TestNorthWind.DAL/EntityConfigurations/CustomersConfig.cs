﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.DAL.EntityConfigurations
{
    public class CustomersConfig : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customers", "dbo");
            builder.Property(p => p.CompanyName).HasColumnType("nvarchar(50)");
            builder.Property(p => p.CustomerId).ValueGeneratedOnAdd();
            builder.HasKey(p => p.CustomerId);
        }
    }
}
