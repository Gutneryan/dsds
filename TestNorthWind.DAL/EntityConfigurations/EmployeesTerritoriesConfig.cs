﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.DAL.EntityConfigurations
{
    public class EmployeesTerritoriesConfig : IEntityTypeConfiguration<EmployeeTerritory>
    {
        public void Configure(EntityTypeBuilder<EmployeeTerritory> builder)
        {
            builder.ToTable("EmployeesTerritories", "dbo");

            builder.HasOne(p => p.Territories)
                .WithMany(p => p.EmployeesTerritories)
                .HasForeignKey(p => p.TerritoryId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(p => p.EmployeeId).ValueGeneratedOnAdd();
            builder.HasKey(p => p.EmployeeId);
        }
    }
}
