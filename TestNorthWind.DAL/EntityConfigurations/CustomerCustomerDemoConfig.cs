﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.DAL.EntityConfigurations
{
    public class CustomerCustomerDemoConfig : IEntityTypeConfiguration<CustomerCustomerDemo>
    {
        public void Configure(EntityTypeBuilder<CustomerCustomerDemo> builder)
        {
            builder.ToTable("CustomerCustomerDemo", "dbo");

            builder.HasKey(p => new { p.CustomerId, p.CustomertypeId });

            builder.HasOne(p => p.Customers)
                .WithMany(p => p.CustomerCustomerDemo)
                .HasForeignKey(p => p.CustomerId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.CustomersType)
                .WithMany(p => p.CustomerCustomerDemo)
                .HasForeignKey(p => p.CustomertypeId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
