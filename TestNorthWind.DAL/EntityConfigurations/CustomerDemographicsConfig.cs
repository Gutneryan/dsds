﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.DAL.EntityConfigurations
{
    public class CustomerDemographicsConfig : IEntityTypeConfiguration<CustomerDemographic>
    {
        public void Configure(EntityTypeBuilder<CustomerDemographic> builder)
        {
            builder.ToTable("CustomerDemographics", "dbo");

            builder.Property(p => p.CustomerTypeId).ValueGeneratedOnAdd();

            builder.HasKey(p => p.CustomerTypeId);
        }
    }
}
