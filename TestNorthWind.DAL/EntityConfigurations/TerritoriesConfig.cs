﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.DAL.EntityConfigurations
{
    public class TerritoriesConfig : IEntityTypeConfiguration<Territory>
    {
        public void Configure(EntityTypeBuilder<Territory> builder)
        {
            builder.ToTable("Territories", "dbo");
            builder.Property(p => p.TerritoryId).ValueGeneratedOnAdd();
            builder.Property(p => p.TerritoryDescription).HasColumnType("nvarchar(100)");
            builder.HasKey(p => p.TerritoryId);

            builder.HasOne(p => p.Region)
                .WithMany(p => p.Territories)
                .HasForeignKey(p => p.RegionId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
