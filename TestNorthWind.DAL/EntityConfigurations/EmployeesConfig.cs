﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.DAL.EntityConfigurations
{
    public class EmployeesConfig : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.ToTable("Employees", "dbo");
            builder.Property(p => p.EmployeeId).ValueGeneratedOnAdd();
            builder.Property(p => p.Address).HasColumnType("nvarchar(50)");
            builder.Property(p => p.BirthDate).HasColumnType("nvarchar(50)");
            builder.Property(p => p.City).HasColumnType("nvarchar(50)");
            builder.Property(p => p.Country).HasColumnType("nvarchar(50)");
            builder.Property(p => p.Extension).HasColumnType("nvarchar(50)");
            builder.Property(p => p.FirstName).HasColumnType("nvarchar(50)");
            builder.Property(p => p.HireDate).HasColumnType("nvarchar(50)");
            builder.Property(p => p.HomePhone).HasColumnType("nvarchar(50)");
            builder.Property(p => p.LastName).HasColumnType("nvarchar(50)");
            builder.Property(p => p.Notes).HasColumnType("nvarchar(50)");
            builder.Property(p => p.PostalCode).HasColumnType("nvarchar(50)");
            builder.Property(p => p.Region).HasColumnType("nvarchar(50)");
            builder.Property(p => p.Title).HasColumnType("nvarchar(50)");
            builder.Property(p => p.TitleOfCourtesy).HasColumnType("nvarchar(50)");
            builder.HasKey(p => p.EmployeeId);
        }
    }
}
