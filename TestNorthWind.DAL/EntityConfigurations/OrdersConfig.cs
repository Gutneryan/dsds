﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.DAL.EntityConfigurations
{
    public class OrdersConfig : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Orders", "dbo");
            builder.Property(p => p.OrderId).ValueGeneratedOnAdd();
            builder.HasKey(p => p.OrderId);
            builder.Property(p => p.ShipAddress).HasColumnType("nvarchar(50)");
            builder.Property(p => p.ShipCity).HasColumnType("nvarchar(50)");
            builder.Property(p => p.ShipCountry).HasColumnType("nvarchar(50)");
            builder.Property(p => p.ShipName).HasColumnType("nvarchar(50)");
            builder.Property(p => p.ShipPostalCode).HasColumnType("nvarchar(50)");
            builder.Property(p => p.ShipRegion).HasColumnType("nvarchar(50)");
            builder.Property(p => p.ShipRegion).HasColumnType("nvarchar(50)");

            builder.HasOne(p => p.Customers)
                .WithMany(p => p.Orders)
                .HasForeignKey(p => p.CustomerId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.Employees)
                .WithMany(p => p.Orders)
                .HasForeignKey(p => p.OrderId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.Shippers)
                .WithMany(p => p.Orders)
                .HasForeignKey(p => p.OrderId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
