﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.DAL.EntityConfigurations
{
    public class ProductsConfig : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Products", "dbo");

            builder.HasKey(p => p.ProductId);

            builder.Property(p => p.ProductId).ValueGeneratedOnAdd();

            builder.Property(p => p.ProductName).HasColumnType("nvarchar(50)");

            builder.Property(p => p.QuantityPerUnit).HasColumnType("nvarchar(50)");

            builder.HasOne(p => p.Suppliers)
                .WithMany(p => p.Products)
                .HasForeignKey(p => p.SupplierId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(p => p.Categories)
                    .WithMany(p => p.Products)
                    .HasForeignKey(p => p.ProductId)
                    .OnDelete(DeleteBehavior.Restrict);


        }
    }
}
