﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.DAL.EntityConfigurations
{
    public class ShippersConfig : IEntityTypeConfiguration<Shipper>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Shipper> builder)
        {
            builder.ToTable("Shippers", "dbo");
            builder.Property(p => p.ShipperId).ValueGeneratedOnAdd();
            builder.HasKey(p => p.ShipperId);
            builder.Property(p => p.CompanyName).HasColumnType("nvarchar(50)");
            builder.Property(p => p.Phone).HasColumnType("nvarchar(50)");
        }
    }
}
