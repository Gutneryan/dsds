﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.DAL.EntityConfigurations
{
    public class CategoriesConfig : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Categories", "dbo");

            builder.Property(P => P.CategoryId).ValueGeneratedOnAdd();
            
            builder.Property(p => p.CategoryName).HasColumnType("nvarchar(50)");

            builder.HasKey(p => p.CategoryId);
        }
    }
}
