﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.DAL.EntityConfigurations
{
    public class RegionConfig : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.ToTable("Region", "dbo");
            builder.Property(p => p.RegionId).ValueGeneratedOnAdd();
            builder.HasKey(p => p.RegionId);
            builder.Property(p => p.RegionDescription).HasColumnType("nvarchar(100)");

        }
    }
}
