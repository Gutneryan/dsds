﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.DAL.EntityConfigurations
{
    public class SuppliersConfig : IEntityTypeConfiguration<Supplier>
    {
        public void Configure(EntityTypeBuilder<Supplier> builder)
        {
            builder.ToTable("Suppliers", "dbo");

            builder.Property(p => p.SupplierId).ValueGeneratedOnAdd();

            builder.HasKey(p => p.SupplierId);

            builder.Property(p => p.Address).HasColumnType("nvarchar(50)");
            builder.Property(p => p.City).HasColumnType("nvarchar(50)");
            builder.Property(p => p.CompanyName).HasColumnType("nvarchar(50)");
            builder.Property(p => p.ContactName).HasColumnType("nvarchar(50)");
            builder.Property(p => p.ContactTitle).HasColumnType("nvarchar(50)");
            builder.Property(p => p.Country).HasColumnType("nvarchar(50)");
            builder.Property(p => p.HomePage).HasColumnType("nvarchar(50)");
            builder.Property(p => p.Phone).HasColumnType("nvarchar(50)");
            builder.Property(p => p.PostalCode).HasColumnType("nvarchar(50)");
            builder.Property(p => p.Region).HasColumnType("nvarchar(50)");
        }
    }
}
