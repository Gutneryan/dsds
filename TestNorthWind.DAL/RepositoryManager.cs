﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestNorthWind.Core;
using TestNorthWind.Core.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace TestNorthWind.DAL
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly TNWContext _context;
        public RepositoryManager(IServiceProvider serviceProvider, TNWContext context)
        {
            _serviceProvider = serviceProvider;
            _context = context;
        }

        private IEmployeesRepository _employees;
        public IEmployeesRepository Employees => _employees ?? (_employees=_serviceProvider.GetService<IEmployeesRepository>());

        private IEmployeesTerritoriesRepository _employeesTerritories;
        public IEmployeesTerritoriesRepository EmployeesTerritories => _employeesTerritories ?? (_employeesTerritories = _serviceProvider.GetService<IEmployeesTerritoriesRepository>());

        private ICustomerCustomerDemoRepository _customerCustomerDemo;
        public ICustomerCustomerDemoRepository CustomerCustomerDemo => _customerCustomerDemo ?? (_customerCustomerDemo = _serviceProvider.GetService<ICustomerCustomerDemoRepository>());

        private ICustomerDemographicsRepository _customerDemographicsRepository;
        public ICustomerDemographicsRepository CustomerDemographics => _customerDemographicsRepository ?? (_customerDemographicsRepository = _serviceProvider.GetService<ICustomerDemographicsRepository>());

        private ICustomersRepository _customers;
        public ICustomersRepository Customers => _customers ?? (_customers = _serviceProvider.GetService<ICustomersRepository>());

        private IOrderDetailsRepository _orderDetails;
        public IOrderDetailsRepository OrderDetails => _orderDetails ?? (_orderDetails = _serviceProvider.GetService<IOrderDetailsRepository>());

        private IOrdersRepository _orders;
        public IOrdersRepository Orders => _orders ?? (_orders = _serviceProvider.GetService<IOrdersRepository>());

        private IProductsRepository _products;
        public IProductsRepository Products => _products ?? (_products = _serviceProvider.GetService<IProductsRepository>());

        private IRegionRepository _region;
        public IRegionRepository Region => _region ?? (_region = _serviceProvider.GetService<IRegionRepository>());

        private IShippersRepository _shippers;
        public IShippersRepository Shippers => _shippers ?? (_shippers = _serviceProvider.GetService<IShippersRepository>());

        private ISuppliersRepository _suppliers;
        public ISuppliersRepository Suppliers => _suppliers ?? (_suppliers = _serviceProvider.GetService<ISuppliersRepository>());

        private ITerritoriesRepository _territories;
        public ITerritoriesRepository Territories => _territories ?? (_territories = _serviceProvider.GetService<ITerritoriesRepository>());

        private ICategoriesRepository _categories;
        public ICategoriesRepository Categories => _categories ?? (_categories = _serviceProvider.GetService<ICategoriesRepository>());

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
    }
}
