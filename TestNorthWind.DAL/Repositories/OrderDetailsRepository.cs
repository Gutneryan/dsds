﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class OrderDetailsRepository : EntityBaseRepository<OrderDetail>, IOrderDetailsRepository
    {
        public OrderDetailsRepository(TNWContext context) : base(context)
        {

        }
    }
}
