﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class ProductsRepository : EntityBaseRepository<Product>, IProductsRepository
    {
        public ProductsRepository(TNWContext context) : base(context)
        {

        }
    }
}
