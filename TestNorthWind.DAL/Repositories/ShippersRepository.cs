﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class ShippersRepository : EntityBaseRepository<Shipper>, IShippersRepository
    {
        public ShippersRepository(TNWContext context) : base(context)
        {

        }
    }
}
