﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class OrdersRepository : EntityBaseRepository<Order>, IOrdersRepository
    {
        public OrdersRepository(TNWContext context) : base(context)
        {

        }
    }
}
