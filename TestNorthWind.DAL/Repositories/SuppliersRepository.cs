﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class SuppliersRepository : EntityBaseRepository<Supplier>, ISuppliersRepository
    {
        public SuppliersRepository(TNWContext context) : base(context)
        {

        }
    }
}
