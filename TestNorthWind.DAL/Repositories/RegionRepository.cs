﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class RegionRepository : EntityBaseRepository<Region>, IRegionRepository
    {
        public RegionRepository(TNWContext context) : base(context)
        {

        }
    }
}
