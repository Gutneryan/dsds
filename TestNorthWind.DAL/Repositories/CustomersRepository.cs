﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class CustomersRepository : EntityBaseRepository<Customer>, ICustomersRepository
    {
        public CustomersRepository(TNWContext context) : base(context)
        {

        }
    }
}
