﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class EntityBaseRepository<T> : IEntityBaseRepository<T> where T :class, new()
    {
        protected readonly TNWContext _tNWContext;
        public EntityBaseRepository(TNWContext tNWContext)
        {
            _tNWContext = tNWContext;
        }
        public T Add(T entity)
        {
            _tNWContext.Set<T>().Add(entity);
            return entity;
        }

        public async Task<IEnumerable<T>> AllIncludingAsync(System.Linq.Expressions.Expression<Func<T, bool>> whereProperties, params System.Linq.Expressions.Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _tNWContext.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            if (whereProperties != null)
                query = query.Where(whereProperties);
            return await query.ToListAsync();
        }

        public void Commit()
        {
            _tNWContext.SaveChanges();
        }

        public async Task<int> CountAsync()
        {
            return await _tNWContext.Set<T>().CountAsync();
        }

        public void Delete(T entity)
        {
            EntityEntry dbEntityEntry = _tNWContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }

        public virtual void DeleteWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            IEnumerable<T> entities = _tNWContext.Set<T>().Where(predicate);

            foreach (var entity in entities)
            {
                _tNWContext.Entry<T>(entity).State = EntityState.Deleted;
            }
        }

        public virtual IEnumerable<T> Filter(Core.FilterBase<T> b)
        {
            return b.Filter(_tNWContext.Set<T>());
        }

        public async Task<IList<T>> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return await _tNWContext.Set<T>().Where(predicate).ToListAsync();
        }

        public async Task<IList<T>> GetAllAsync()
        {
            return await _tNWContext.Set<T>().ToListAsync();
        }

        public async Task<IList<T>> GetAllAsync(System.Linq.Expressions.Expression<Func<T, bool>> whereExp, params System.Linq.Expressions.Expression<Func<T, object>>[] includeExps)
        {
            IQueryable<T> query = _tNWContext.Set<T>();
            query = query.Where(whereExp);
            if (includeExps != null)
                query = includeExps.Aggregate(query, (current, exp) => current.Include(exp));

            return await query.ToListAsync();
        }

        public async Task<IList<TProp>> GetAndSelectSingle<TProp>(System.Linq.Expressions.Expression<Func<T, bool>> predicate, System.Linq.Expressions.Expression<Func<T, TProp>> selectProperty)
        {
            return await _tNWContext.Set<T>().Where(predicate).Select(selectProperty).ToListAsync();
        }

        //public async Task<T> GetSingle(long id)
        //{
        //    return await _tNWContext.Set<T>().FirstOrDefaultAsync(x => x.Id.Equals(id));
        //}

        public async Task<T> GetSingle(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return await _tNWContext.Set<T>().FirstOrDefaultAsync(predicate);
        }

        public async Task<T> GetSingle(System.Linq.Expressions.Expression<Func<T, bool>> predicate, params System.Linq.Expressions.Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _tNWContext.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return await query.Where(predicate).FirstOrDefaultAsync();
        }

        public async Task<bool> IfAll(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return await _tNWContext.Set<T>().AllAsync(predicate);
        }

        public async Task<bool> IfAny(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return await _tNWContext.Set<T>().AnyAsync(predicate);
        }

        public void Reload(T entity)
        {
            _tNWContext.Entry(entity).Reload();
        }

        public void Update(T entity)
        {
            EntityEntry dbEntityEntry = _tNWContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Modified;
        }
    }
}
