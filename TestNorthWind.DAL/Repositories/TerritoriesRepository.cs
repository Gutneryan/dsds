﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class TerritoriesRepository : EntityBaseRepository<Territory>, ITerritoriesRepository
    {
        public TerritoriesRepository(TNWContext context) : base(context)
        {

        }
    }
}
