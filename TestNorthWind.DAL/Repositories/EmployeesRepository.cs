﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class EmployeesRepository : EntityBaseRepository<Employee>, IEmployeesRepository
    {
        public EmployeesRepository(TNWContext context) : base(context)
        {

        }
    }
}
