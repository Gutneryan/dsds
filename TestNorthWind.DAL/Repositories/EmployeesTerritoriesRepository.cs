﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class EmployeesTerritoriesRepository : EntityBaseRepository<EmployeeTerritory>, IEmployeesTerritoriesRepository
    {
        public EmployeesTerritoriesRepository(TNWContext context) : base(context)
        {

        }
    }
}
