﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class CategoriesRepository : EntityBaseRepository<Category>, ICategoriesRepository
    {
        public CategoriesRepository(TNWContext context):base(context)
        {
              
        }
    }
}
