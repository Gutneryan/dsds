﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.DAL.Repositories
{
    public class CustomerDemographicsRepository : EntityBaseRepository<CustomerDemographic>, ICustomerDemographicsRepository
    {
        public CustomerDemographicsRepository(TNWContext context) : base(context)
        {

        }
    }
}
