﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestNorthWind.ViewModels
{
    public class CustomerViewModel
    {
        public long CustomerId { get; set; }
        [Required]
        public string CompanyName { get; set; }
    }
}
