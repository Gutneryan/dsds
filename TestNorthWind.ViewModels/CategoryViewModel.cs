﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNorthWind.ViewModels
{
    public class CategoryViewModel
    {
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public byte[] Picture { get; set; }
    }
}
