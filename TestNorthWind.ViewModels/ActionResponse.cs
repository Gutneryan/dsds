﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNorthWind.ViewModels
{
    public class ActionResponse
    {
        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>The status code.</value>
        public ResponseStatus StatusCode { get; set; }
        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage { get; set; }
        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>The result.</value>
        public object Result { get; set; }

        public ActionResponse()
        {

        }
        public ActionResponse(ResponseStatus statusCode, string errorMessage = null)
        {
            StatusCode = statusCode;
            ErrorMessage = errorMessage;
        }
    }

    /// <summary>
    /// Enum ResponseStatus
    /// </summary>
    public enum ResponseStatus
    {
        /// <summary>
        /// The error
        /// </summary>
        Error,

        /// <summary>
        /// The ok
        /// </summary>
        Ok
    }
}
