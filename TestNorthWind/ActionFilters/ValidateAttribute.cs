﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestNorthWind.ViewModels;

namespace TestNorthWind.ActionFilters
{
    public class ValidateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var modelState = context.ModelState;

            if (modelState.IsValid)
                return;

            var errMessage = modelState.SelectMany(ms => ms.Value.Errors).Aggregate("", (current, err) => current + " " + err.ErrorMessage);
            var response = new ActionResponse
            {
                StatusCode = ResponseStatus.Error,
                ErrorMessage = errMessage
            };
            context.HttpContext.Response.StatusCode = 400;
            context.HttpContext.Response.Headers.Clear();
            context.Result = new JsonResult(response);
        }
    }
}
