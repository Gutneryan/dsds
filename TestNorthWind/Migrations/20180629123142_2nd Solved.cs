﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TestNorthWind.Migrations
{
    public partial class _2ndSolved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                schema: "dbo",
                table: "CustomerDemographics");

            migrationBuilder.DropColumn(
                name: "City",
                schema: "dbo",
                table: "CustomerDemographics");

            migrationBuilder.DropColumn(
                name: "ContactTitle",
                schema: "dbo",
                table: "CustomerDemographics");

            migrationBuilder.DropColumn(
                name: "Contactname",
                schema: "dbo",
                table: "CustomerDemographics");

            migrationBuilder.DropColumn(
                name: "Country",
                schema: "dbo",
                table: "CustomerDemographics");

            migrationBuilder.DropColumn(
                name: "Fax",
                schema: "dbo",
                table: "CustomerDemographics");

            migrationBuilder.DropColumn(
                name: "Phone",
                schema: "dbo",
                table: "CustomerDemographics");

            migrationBuilder.DropColumn(
                name: "PostalCode",
                schema: "dbo",
                table: "CustomerDemographics");

            migrationBuilder.DropColumn(
                name: "Region",
                schema: "dbo",
                table: "CustomerDemographics");

            migrationBuilder.AlterColumn<string>(
                name: "CustomerDesc",
                schema: "dbo",
                table: "CustomerDemographics",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(MAX)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "CustomerDesc",
                schema: "dbo",
                table: "CustomerDemographics",
                type: "nvarchar(MAX)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address",
                schema: "dbo",
                table: "CustomerDemographics",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                schema: "dbo",
                table: "CustomerDemographics",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactTitle",
                schema: "dbo",
                table: "CustomerDemographics",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Contactname",
                schema: "dbo",
                table: "CustomerDemographics",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                schema: "dbo",
                table: "CustomerDemographics",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Fax",
                schema: "dbo",
                table: "CustomerDemographics",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                schema: "dbo",
                table: "CustomerDemographics",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalCode",
                schema: "dbo",
                table: "CustomerDemographics",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Region",
                schema: "dbo",
                table: "CustomerDemographics",
                type: "nvarchar(50)",
                nullable: true);
        }
    }
}
