﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TestNorthWind.Migrations
{
    public partial class Solved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerCustomerDemo_CustomersConfig_CustomerId",
                schema: "dbo",
                table: "CustomerCustomerDemo");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_CustomersConfig_CustomerId",
                schema: "dbo",
                table: "Orders");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CustomersConfig",
                schema: "dbo",
                table: "CustomersConfig");

            migrationBuilder.RenameTable(
                name: "CustomersConfig",
                schema: "dbo",
                newName: "Customers");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Customers",
                schema: "dbo",
                table: "Customers",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerCustomerDemo_Customers_CustomerId",
                schema: "dbo",
                table: "CustomerCustomerDemo",
                column: "CustomerId",
                principalSchema: "dbo",
                principalTable: "Customers",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Customers_CustomerId",
                schema: "dbo",
                table: "Orders",
                column: "CustomerId",
                principalSchema: "dbo",
                principalTable: "Customers",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerCustomerDemo_Customers_CustomerId",
                schema: "dbo",
                table: "CustomerCustomerDemo");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Customers_CustomerId",
                schema: "dbo",
                table: "Orders");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Customers",
                schema: "dbo",
                table: "Customers");

            migrationBuilder.RenameTable(
                name: "Customers",
                schema: "dbo",
                newName: "CustomersConfig");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CustomersConfig",
                schema: "dbo",
                table: "CustomersConfig",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerCustomerDemo_CustomersConfig_CustomerId",
                schema: "dbo",
                table: "CustomerCustomerDemo",
                column: "CustomerId",
                principalSchema: "dbo",
                principalTable: "CustomersConfig",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_CustomersConfig_CustomerId",
                schema: "dbo",
                table: "Orders",
                column: "CustomerId",
                principalSchema: "dbo",
                principalTable: "CustomersConfig",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
