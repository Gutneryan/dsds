﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using TestNorthWind.DAL;

namespace TestNorthWind.Migrations
{
    [DbContext(typeof(TNWContext))]
    [Migration("20180629064443_Init")]
    partial class Init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.2-rtm-10011")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TestNorthWind.Core.Entities.Category", b =>
                {
                    b.Property<long>("CategoryId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CategoryName")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Description");

                    b.Property<byte[]>("Picture");

                    b.HasKey("CategoryId");

                    b.ToTable("Categories","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.Customer", b =>
                {
                    b.Property<long>("CustomerId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CompanyName")
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("CustomerId");

                    b.ToTable("CustomersConfig","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.CustomerCustomerDemo", b =>
                {
                    b.Property<long>("CustomerId");

                    b.Property<long>("CustomertypeId");

                    b.HasKey("CustomerId", "CustomertypeId");

                    b.HasIndex("CustomertypeId");

                    b.ToTable("CustomerCustomerDemo","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.CustomerDemographic", b =>
                {
                    b.Property<long>("CustomerTypeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("City")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("ContactTitle")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Contactname")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Country")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("CustomerDesc")
                        .HasColumnType("nvarchar(MAX)");

                    b.Property<string>("Fax");

                    b.Property<string>("Phone")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("PostalCode")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Region")
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("CustomerTypeId");

                    b.ToTable("CustomerDemographics","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.Employee", b =>
                {
                    b.Property<long>("EmployeeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("BirthDate")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("City")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Country")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Extension")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("HireDate")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("HomePhone")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Notes")
                        .HasColumnType("nvarchar(50)");

                    b.Property<byte[]>("Photo");

                    b.Property<string>("PhotoPath");

                    b.Property<string>("PostalCode")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Region")
                        .HasColumnType("nvarchar(50)");

                    b.Property<int?>("ReportsTo");

                    b.Property<string>("Title")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("TitleOfCourtesy")
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("EmployeeId");

                    b.ToTable("Employees","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.EmployeeTerritory", b =>
                {
                    b.Property<long>("EmployeeId")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("TerritoryId");

                    b.HasKey("EmployeeId");

                    b.HasIndex("TerritoryId");

                    b.ToTable("EmployeesTerritories","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.Order", b =>
                {
                    b.Property<long>("OrderId")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CustomerId");

                    b.Property<long>("EmployeeId");

                    b.Property<double>("Freight");

                    b.Property<DateTime>("OrderDate");

                    b.Property<DateTime>("RequiredDate");

                    b.Property<string>("ShipAddress")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("ShipCity")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("ShipCountry")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("ShipName")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("ShipPostalCode")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("ShipRegion")
                        .HasColumnType("nvarchar(50)");

                    b.Property<long>("ShipVia");

                    b.Property<DateTime>("ShippedDate");

                    b.HasKey("OrderId");

                    b.HasIndex("CustomerId");

                    b.ToTable("Orders","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.OrderDetail", b =>
                {
                    b.Property<long>("ProductId");

                    b.Property<long>("OrderId");

                    b.Property<int>("Discount");

                    b.Property<int>("Quantity");

                    b.Property<decimal>("UnitPrice");

                    b.HasKey("ProductId", "OrderId");

                    b.HasIndex("OrderId");

                    b.ToTable("OrderDetails","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.Product", b =>
                {
                    b.Property<long>("ProductId")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("CategoryId");

                    b.Property<int>("Discontinued");

                    b.Property<string>("ProductName")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("QuantityPerUnit")
                        .HasColumnType("nvarchar(50)");

                    b.Property<int>("ReorderLevel");

                    b.Property<long>("SupplierId");

                    b.Property<decimal>("UnitPrice");

                    b.Property<int>("UnitsInStock");

                    b.Property<int>("UnitsOnOrder");

                    b.HasKey("ProductId");

                    b.HasIndex("SupplierId");

                    b.ToTable("Products","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.Region", b =>
                {
                    b.Property<long>("RegionId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("RegionDescription")
                        .HasColumnType("nvarchar(100)");

                    b.HasKey("RegionId");

                    b.ToTable("Region","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.Shipper", b =>
                {
                    b.Property<long>("ShipperId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CompanyName")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Phone")
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("ShipperId");

                    b.ToTable("Shippers","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.Supplier", b =>
                {
                    b.Property<long>("SupplierId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("City")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("CompanyName")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("ContactName")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("ContactTitle")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Country")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Fax");

                    b.Property<string>("HomePage")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Phone")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("PostalCode")
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Region")
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("SupplierId");

                    b.ToTable("Suppliers","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.Territory", b =>
                {
                    b.Property<long>("TerritoryId")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("RegionId");

                    b.Property<string>("TerritoryDescription")
                        .HasColumnType("nvarchar(100)");

                    b.HasKey("TerritoryId");

                    b.HasIndex("RegionId");

                    b.ToTable("Territories","dbo");
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.CustomerCustomerDemo", b =>
                {
                    b.HasOne("TestNorthWind.Core.Entities.Customer", "Customers")
                        .WithMany("CustomerCustomerDemo")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("TestNorthWind.Core.Entities.CustomerDemographic", "CustomersType")
                        .WithMany("CustomerCustomerDemo")
                        .HasForeignKey("CustomertypeId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.EmployeeTerritory", b =>
                {
                    b.HasOne("TestNorthWind.Core.Entities.Territory", "Territories")
                        .WithMany("EmployeesTerritories")
                        .HasForeignKey("TerritoryId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.Order", b =>
                {
                    b.HasOne("TestNorthWind.Core.Entities.Customer", "Customers")
                        .WithMany("Orders")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("TestNorthWind.Core.Entities.Employee", "Employees")
                        .WithMany("Orders")
                        .HasForeignKey("OrderId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("TestNorthWind.Core.Entities.Shipper", "Shippers")
                        .WithMany("Orders")
                        .HasForeignKey("OrderId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.OrderDetail", b =>
                {
                    b.HasOne("TestNorthWind.Core.Entities.Order", "Orders")
                        .WithMany("OrderDetails")
                        .HasForeignKey("OrderId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("TestNorthWind.Core.Entities.Product", "Products")
                        .WithMany("OrderDetails")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.Product", b =>
                {
                    b.HasOne("TestNorthWind.Core.Entities.Category", "Categories")
                        .WithMany("Products")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("TestNorthWind.Core.Entities.Supplier", "Suppliers")
                        .WithMany("Products")
                        .HasForeignKey("SupplierId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("TestNorthWind.Core.Entities.Territory", b =>
                {
                    b.HasOne("TestNorthWind.Core.Entities.Region", "Region")
                        .WithMany("Territories")
                        .HasForeignKey("RegionId")
                        .OnDelete(DeleteBehavior.Restrict);
                });
#pragma warning restore 612, 618
        }
    }
}
