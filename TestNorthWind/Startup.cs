﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using TestNorthWind.ActionFilters;
using TestNorthWind.Core;
using TestNorthWind.DAL;
using TestNorthWind.ErrorHandling;

namespace TestNorthWind
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TNWContext>(options => options.UseSqlServer(Configuration["Data:TNWConnection:ConnectionString"],
                b => b.MigrationsAssembly("TestNorthWind")
                ));
            services.AddRepositoriesAndBussinesLayerServices();
            services.AddMvc(x =>
            {
                x.Filters.Add(new ValidateAttribute());
            }
            );
            services.AddAutoMapper();
            services.AddTransient<IRepositoryManager, RepositoryManager>();
            services.AddSwaggerGen(c =>
            {
                c.DescribeAllEnumsAsStrings();
                c.SwaggerDoc("v1", new Info { Title = Configuration.GetSection("Swagger")["Description"], Version = "v1" });

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseSwagger();
            var swaggerConfig = Configuration.GetSection("Swagger");

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(swaggerConfig["Endpoint"], swaggerConfig["Description"]);

            });
            app.UseMvc();

        }
    }
}
