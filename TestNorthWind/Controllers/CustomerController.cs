﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestNorthWind.Core.BussinessLayerInterfaces;
using TestNorthWind.Core.Entities;
using TestNorthWind.ViewModels;

namespace TestNorthWind.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ICustomerBL _customerBL;

        public CustomerController(IMapper mapper, ICustomerBL categoryBL)
        {
            _mapper = mapper;
            _customerBL = categoryBL;
        }

        [HttpGet]
        public async Task<ActionResponse> GetCustomers()
        {
            var customers = await _customerBL.GetAll();

            var result = _mapper.Map<IEnumerable<CustomerViewModel>>(customers);

            return new ActionResponse
            {
                StatusCode = ResponseStatus.Ok,
                Result = result
            };

        }
        [HttpGet("{id}")]
        public async Task<ActionResponse> GetCustomer(long id)
        {
            var customer = await _customerBL.GetCustomer(id);

            var result = _mapper.Map<CustomerViewModel>(customer);

            return new ActionResponse
            {
                StatusCode = ResponseStatus.Ok,
                Result = result
            };
        }
        [HttpPost]
        public async Task<ActionResponse> AddCustomer([FromBody]Customer customerCustomerViewModel)
        {
            //var customer = new Customer
            //{
            //    CompanyName = customerCustomerViewModel.CompanyName
            //};
            var customer = new CustomerCustomerDemographicsModel
            {
                CompanyName = customerCustomerViewModel.CompanyName,
                CustomerDesc = customerCustomerViewModel.CustomerDesc,
            };

            await _customerBL.AddCustomer(customer);
            return new ActionResponse
            {
                StatusCode = ResponseStatus.Ok,
            };

        }

        [HttpPut("{id}")]
        public async Task<ActionResponse> EditCustomer(long Id,[FromBody]CustomerViewModel customerViewModel)
        {
            var result = new Customer
            {
                CompanyName=customerViewModel.CompanyName,
            };
            await _customerBL.EditCustomer(Id,result);

            return new ActionResponse
            {
                StatusCode = ResponseStatus.Ok,
            };
        }
        [HttpDelete("{id}")]
        public async Task<ActionResponse> DeleteCategory(long Id)
        {
            await _customerBL.DeleteCustomer(Id);
            return new ActionResponse
            {
                StatusCode = ResponseStatus.Ok,
            };
        }
    }
}