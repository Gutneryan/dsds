﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestNorthWind.Core.BussinessLayerInterfaces;
using TestNorthWind.Core.Entities;
using TestNorthWind.ViewModels;

namespace TestNorthWind.Controllers
{
    [Route("api/[controller]")]
    public class CategoriesController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ICategoryBL _categoryBL;

        public CategoriesController(IMapper mapper, ICategoryBL category)
        {
            _mapper = mapper;
            _categoryBL = category;
        }

        [HttpGet]
        public async Task<ActionResponse> GetCategories()
        {
            var categories = await _categoryBL.GetAll();

            var result = _mapper.Map<IEnumerable<CategoryViewModel>>(categories);

            return new ActionResponse
            {
                StatusCode = ResponseStatus.Ok,
                Result = result
            };

        }
        [HttpGet("{id}")]
        public async Task<ActionResponse> GetCategory(long id)
        {
            var categories = await _categoryBL.GetCategory(id);

            var result = _mapper.Map<CategoryViewModel>(categories);

            return new ActionResponse
            {
                StatusCode = ResponseStatus.Ok,
                Result = result
            };

        }

        [HttpPost]
        public async Task<ActionResponse> AddCategory([FromBody]CategoryViewModel categoryViewModel)
        {
            var category = new Category
            {
                CategoryName = categoryViewModel.CategoryName,
                Description = categoryViewModel.Description,
                Picture = categoryViewModel.Picture
            };
            await _categoryBL.AddCategory(category);
            return new ActionResponse
            {
                StatusCode = ResponseStatus.Ok,
            };
        }

        [HttpPut("{id}")]
        public async Task<ActionResponse> EditCategory(long Id,[FromBody]CategoryViewModel categoryViewModel)
        {
            var result = new Category
            {
                CategoryName = categoryViewModel.CategoryName,
                Description = categoryViewModel.Description,
                Picture = categoryViewModel.Picture
            };

            await _categoryBL.EditCategory(Id,result);
            return new ActionResponse
            {
                StatusCode = ResponseStatus.Ok,
            };
        }

        [HttpDelete("{id}")]
        public async Task<ActionResponse> DeleteCategory(long Id)
        {

            await _categoryBL.DeleteCategory(Id);
            return new ActionResponse
            {
                StatusCode = ResponseStatus.Ok,
            };
        }
    }
}