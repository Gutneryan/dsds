﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace TestNorthWind.Core
{
    public static class DependancyConfiguration
    {
        private const string DALAssebmlyName = "TestNorthWind.DAL";
        private const string BLLAssemblyName = "TestNorthWind.BLL";
        private const string RepositoryServiceNamespace = "TestNorthWind.Core.Repositories";
        private const string BussinesLogicServiceNamespace = "TestNorthWind.Core.BussinessLayerInterfaces";
        private const string BussinesLogicNamespace = "TestNorthWind.BLL";
        private const string RepositoriesNamespace = "TestNorthWind.DAL.Repositories";
        public static void AddRepositoriesAndBussinesLayerServices(this IServiceCollection services)
        {
            services.AddRepositories();
            services.AddBussinesLayerServices();
        }
        public static void AddRepositories(this IServiceCollection services)


        {
            var dalAsm = Assembly.Load(new AssemblyName(DALAssebmlyName));

            foreach (var entry in RepositoryServiceAndImplementationTypes)
            {
                var serviceType = Type.GetType($"{RepositoryServiceNamespace}.{entry.Key}");
                var implementationType = dalAsm.GetType($"{RepositoriesNamespace}.{entry.Value}");

                services.Add(new ServiceDescriptor(serviceType, implementationType, ServiceLifetime.Transient));
            }
        }
        public static void AddBussinesLayerServices(this IServiceCollection services)
        {
            var blAsm = Assembly.Load(new AssemblyName(BLLAssemblyName));

            foreach (var entry in BussinesLayerServiceAndImplementationTypes)
            {
                var serviceType = Type.GetType($"{BussinesLogicServiceNamespace}.{entry.Key}");
                var implementationType = blAsm.GetType($"{BussinesLogicNamespace}.{entry.Value}");

                services.Add(new ServiceDescriptor(serviceType, implementationType, ServiceLifetime.Transient));
            }
        }
        private static readonly Dictionary<string, string> RepositoryServiceAndImplementationTypes = new Dictionary
            <string, string>
        {
            {"ICategoriesRepository","CategoriesRepository" },
            {"ICustomerCustomerDemoRepository", "CustomerCustomerDemoRepository"},
            {"ICustomerDemographicsRepository", "CustomerDemographicsRepository"},
            {"ICustomersRepository", "CustomersRepository"},
            {"IEmployeesRepository", "EmployeesRepository"},
            {"IEmployeesTerritoriesRepository", "EmployeesTerritoriesRepository"},
            {"IOrderDetailsRepository", "OrderDetailsRepository"},
            {"IOrdersRepository", "OrdersRepository"},
            {"IProductsRepository", "ProductsRepository"},
            {"IRegionRepository", "RegionRepository"},
            {"IShippersRepository", "ShippersRepository"},
            {"ISuppliersRepository", "SuppliersRepository"},
            {"ITerritoriesRepository", "TerritoriesRepository"},
        };
        private static readonly Dictionary<string, string> BussinesLayerServiceAndImplementationTypes = new Dictionary
            <string, string>
        {
            {"ICategoryBL", "CategoryBL"},
            {"ICustomerBL","CustomerBL"}
        };
    }
}
