﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.Core.Repositories
{
    public interface IEmployeesTerritoriesRepository: IEntityBaseRepository<EmployeeTerritory>
    {
    }
}
