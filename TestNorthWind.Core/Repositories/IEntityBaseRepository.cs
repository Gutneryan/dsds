﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TestNorthWind.Core.Repositories
{
    public interface IEntityBaseRepository<T> where T : class,  new()
    {
        Task<IEnumerable<T>> AllIncludingAsync(Expression<Func<T, bool>> whereProperties, params Expression<Func<T, object>>[] includeProperties);
        Task<IList<T>> GetAllAsync();
        Task<IList<T>> GetAllAsync(Expression<Func<T, Boolean>> whereExp, params Expression<Func<T, object>>[] includeExps);
        Task<int> CountAsync();
        //Task<T> GetSingle(long id);
        Task<T> GetSingle(Expression<Func<T, bool>> predicate);
        Task<T> GetSingle(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<IList<TProp>> GetAndSelectSingle<TProp>(Expression<Func<T, bool>> predicate, Expression<Func<T, TProp>> selectProperty);
        Task<IList<T>> FindBy(Expression<Func<T, bool>> predicate);
        T Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void DeleteWhere(Expression<Func<T, bool>> predicate);
        IEnumerable<T> Filter(FilterBase<T> b);
        Task<bool> IfAny(Expression<Func<T, bool>> predicate);
        Task<bool> IfAll(Expression<Func<T, bool>> predicate);

        void Reload(T entity);
        void Commit();
    }
}
