﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestNorthWind.Core
{
    public abstract class FilterBase<T> where T : class
    {
        public abstract IEnumerable<T> Filter(IQueryable<T> query);
    }
}
