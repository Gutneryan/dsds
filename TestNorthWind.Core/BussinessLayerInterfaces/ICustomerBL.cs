﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestNorthWind.Core.Entities;
using TestNorthWind.ViewModels;

namespace TestNorthWind.Core.BussinessLayerInterfaces
{
    public interface ICustomerBL
    {
        Task<Customer> GetCustomer(long customer);
        Task AddCustomer(Customer customer);
        Task EditCustomer(long id,Customer customer);
        Task<IEnumerable<Customer>> GetAll();
        Task DeleteCustomer(long customerId);
        Task AddCustomerDemographic(CustomerDemographic customer);

    }
}
