﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestNorthWind.Core.Entities;

namespace TestNorthWind.Core.BussinessLayerInterfaces
{
    public interface ICategoryBL
    {
        Task<Category> GetCategory(long categoryId);
        Task AddCategory(Category categories);
        Task EditCategory(long Id,Category categories);
        Task<IEnumerable<Category>> GetAll();
        Task DeleteCategory(long categoryId);
    }
}
