﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNorthWind.Core.Entities
{
    public class EmployeeTerritory
    {
        public long EmployeeId { get; set; }
        public long TerritoryId { get; set; }
        public Territory Territories { get; set; }
    }
}
