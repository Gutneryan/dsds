﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNorthWind.Core.Entities
{
    public class OrderDetail
    {
        public long OrderId { get; set; }
        public long ProductId { get; set; }
        public Product Products { get; set; }
        public Order Orders { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public int Discount { get; set; }
    }
}
