﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNorthWind.Core.Entities
{
    public class Product
    {
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public long SupplierId { get; set; }
        public Supplier Suppliers { get; set; }
        public long CategoryId { get; set; }
        public Category Categories { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal UnitPrice { get; set; }
        public int UnitsInStock { get; set; }
        public int UnitsOnOrder { get; set; }
        public int ReorderLevel { get; set; }
        public int Discontinued { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
