﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNorthWind.Core.Entities
{
    public class CustomerDemographic
    {
        public long CustomerTypeId { get; set; }
        public string CustomerDesc { get; set; }
        public ICollection<CustomerCustomerDemo> CustomerCustomerDemo { get; set; }
    }
}
