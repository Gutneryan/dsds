﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNorthWind.Core.Entities
{
    public class Region
    {
        public long RegionId { get; set; }
        public string RegionDescription { get; set; }
        public ICollection<Territory> Territories { get; set; }
    }
}
