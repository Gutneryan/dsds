﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNorthWind.Core.Entities
{
    public class Territory
    {
        public long TerritoryId { get; set; }
        public string TerritoryDescription { get; set; }
        public long RegionId { get; set; }
        public Region Region { get; set; }
        public ICollection<EmployeeTerritory> EmployeesTerritories { get; set; }
    }
}
