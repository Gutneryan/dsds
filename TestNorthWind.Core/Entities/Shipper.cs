﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNorthWind.Core.Entities
{
    public class Shipper
    {
        public long ShipperId { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
        public ICollection<Order> Orders { get; set; }

    }
}
