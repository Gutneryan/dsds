﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNorthWind.Core.Entities
{
    public class Customer
    {
        public long CustomerId { get; set; }
        public string CompanyName { get; set; }
        public ICollection<CustomerCustomerDemo> CustomerCustomerDemo { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
