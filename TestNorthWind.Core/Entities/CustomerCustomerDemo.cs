﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNorthWind.Core.Entities
{
    public class CustomerCustomerDemo
    {
        public long CustomerId { get; set; }
        public Customer Customers { get; set; }
        public long CustomertypeId { get; set; }
        public CustomerDemographic CustomersType { get; set; }
    }
}
