﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestNorthWind.Core.Repositories;

namespace TestNorthWind.Core
{
    public interface IRepositoryManager:IDisposable
    {
        IEmployeesRepository Employees { get; }
        IEmployeesTerritoriesRepository EmployeesTerritories { get; }
        ICustomerCustomerDemoRepository CustomerCustomerDemo { get; }
        ICustomerDemographicsRepository CustomerDemographics { get; }
        ICustomersRepository Customers { get; }
        IOrderDetailsRepository OrderDetails { get; }
        IOrdersRepository Orders { get; }
        IProductsRepository Products { get; }
        IRegionRepository Region { get; }
        IShippersRepository Shippers { get; }
        ISuppliersRepository Suppliers { get; }
        ITerritoriesRepository Territories { get; }
        ICategoriesRepository Categories { get; }
        int Complete();
        Task<int> CompleteAsync();
    }
}
