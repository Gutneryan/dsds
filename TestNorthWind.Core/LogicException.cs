﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNorthWind.Core.Enums;

namespace TestNorthWind.Core
{
    public class LogicException : Exception
    {
        public ErrorType? TypeId { get; set; }
        public decimal NumberData { get; set; }
        public LogicException()
        {
        }

        public LogicException(string message)
            : base(message)
        {
        }

        public LogicException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public LogicException(ErrorType errorType)
        {
            TypeId = errorType;
        }
        public LogicException(ErrorType errorType, decimal number)
        {
            TypeId = errorType;
            NumberData = number;
        }
    }

}

