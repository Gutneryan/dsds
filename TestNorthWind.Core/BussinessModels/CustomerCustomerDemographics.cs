﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestNorthWind.ViewModels
{
    public class CustomerCustomerDemographicsModel
    {
        public long CustomerId { get; set; }
        public string CompanyName { get; set; }
        public long CustomerTypeId { get; set; }
        public string CustomerDesc { get; set; }
    }
}
